FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y openssh-server nano curl && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /run/sshd

RUN echo "root:qu!e73xeyFuz!b3yUXJ" | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# CREACION DE DIRECTORIOS Y USUARIOS
RUN mkdir -p /datos/reception_files/companyec/INPUT /datos/reception_files/companyec/BACKUP
RUN useradd reception_files -m -p Dsup-yNi9AXNH7cd7FW
RUN echo "reception_files:Dsup-yNi9AXNH7cd7FW" | chpasswd

# AJUSTE DE PERMISOS PARA LOS DIRECTORIOS DE reception_files
RUN chown -R reception_files: /datos/reception_files/companyec
RUN chmod -R 750 /datos/reception_files/companyec

# CREACION DE DIRECTORIOS PARA data_inspector
RUN mkdir -p /datos/data_inspector/companyec/MAPEOS/dat /datos/data_inspector/companyec/MAPEOS/log
RUN useradd data_inspector -m -p uxKVomJ7tHEvMt!GMaj
RUN echo "data_inspector:uxKVomJ7tHEvMt!GMaj" | chpasswd

# AJUSTE DE PERMISOS PARA LOS DIRECTORIOS DE data_inspector
RUN chown -R data_inspector: /datos/data_inspector/companyec/MAPEOS
RUN chmod -R 770 /datos/data_inspector/companyec/MAPEOS

# PERMISOS PARA data_inspector EN LOS DIRECTORIOS DE reception_files
RUN chown -R reception_files: /datos/reception_files/companyec/INPUT /datos/reception_files/companyec/BACKUP
RUN chmod -R 750 /datos/reception_files/companyec/INPUT /datos/reception_files/companyec/BACKUP

EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]
